using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ventas.Datos; // Si utilizan carpetas para guardar las clase debe llamarlas como Using nombreproyecto.nombreCarpeta
using Ventas.Modelos; // Si utilizan carpetas para guardar las clase debe llamarlas como Using nombreproyecto.nombreCarpeta


/*
    Si va utilizar este codigo debe realizar la referencia norma APA:

     Carcamo Luna, B. (2021). Registro de Productos (Nº de versión 1). Windows. https://gitlab.com/bryan.carcamoluna/registro-productos.

 
 */



namespace Ventas
{
    public class Program
    {
        static void Main(string[] args)
        {

            // Asignacion de Variables
            bool salirMenu = false;
            int opcion = 0;


            do {
                Console.Clear();// Limpiar Pantalla

                Console.WriteLine("******* Menu Principal*******\n\n" + //Las 2n significa dos salto de linea
                    "1. Ingresar Categorias\n"+
                    "2. Ingresar Productos\n" +
                    "3. Asignar Categoria al Producto\n"+
                    "4. Imprimir los Productos Asignados\n"+
                    "5. Salir de la aplicacion\n");


                try {

                    Console.Write("-- Elija una Opcion( 1 al 5 ) : ");
                    opcion = Int32.Parse(Console.ReadLine());

                    switch (opcion)
                    {

                        case 1: // Agregar Categoria
                            Console.Clear();// Limpia la consola
                            AddCategoria();
                           
                            break;
                        case 2: //Agregar Productos
                            Console.Clear();
                            AddProductos();
                            break;
                        case 3: // Asignar Categoria a los productos
                            Console.Clear();
                            AddAsignarCxP();

                            break;
                        case 4: // Imprimir datos
                            Console.Clear();
                            ListarCxP();
                            break;

                        default: // Diferentes a las opciones anteriores

                            salirMenu = true;

                            Console.Clear(); // Limpiar Pantalla
                            Console.WriteLine("El programa ha cerrado");

                            break;


                    }

                }
                catch(Exception e)
                {
                    Console.Write("Se presento el siguiente error : " + e.Message + "\n\n Presione cualquier tecla para continuar");
                    Console.ReadKey(); // captura cualquier tecla
                    Console.Clear();// Limpia la consola

                }

               

            } while (!salirMenu); // Hasta que salir es Verdadero Finaliza el Ciclo

        }


        // Metodo para agregar Categorias
        static void AddCategoria()
        {
            bool subMenu = false;
            do {
               
                char opcion;

                try {

                    if(GuadarDatos.tamanioCategoria() >= 5) // de 0 a 4 hay 5 elementos
                    {
                        Console.Write("No se puede ingresar mas categoria llego al limite del Array,enter para continuar");
                        Console.ReadKey(); // captura cualquier tecla
                        Console.Clear();// Limpia la consola
                        subMenu = true; // salir del ciclo
                        return;
                    }

                    // agregamos el ID automatico


                    Console.Write("Ingrese nombre de la Categoria: ");

                    string nombre = Console.ReadLine();

                    Categorias Catg = new Categorias { IdC = GuadarDatos.tamanioCategoria(),Nombre = nombre};

                    GuadarDatos.GuardarCategoria(Catg);

                    Console.Write("Desea Ingresar Otra Categoria (S/N): ");
                    opcion = Char.Parse(Console.ReadLine());

                    if (opcion=='N' || opcion == 'n'){ subMenu = true; Console.Clear();}




                    }
                catch(Exception e)
                {
                    Console.Write("Se presento el siguiente error : " + e.Message + "\n\n Prsione cualquier tecla para continuar");
                    Console.ReadKey(); // captura cualquier tecla
                    Console.Clear();// Limpia la consola
                }

            
            } while (!subMenu);

        }

        //Metodo para Agregar Productos
        static void AddProductos()
        {
            bool subMenu = false;
            do
            {
               
                char opcion;

                try
                {
                    if (GuadarDatos.tamanioCategoria() == 0) {

                        Console.Write("No hay Categorias Registradas, Vaya a la opcion 1 del Menu (enter): ");
                        Console.ReadKey(); // captura cualquier tecla
                        Console.Clear();// Limpia la consola
                        subMenu = true; // salir del ciclo
                        return;

                    }

                    if (GuadarDatos.tamanioProducto() >=5) // de 0 a 4 hay 5 elementos
                    {
                        Console.Write("No se puede ingresar mas Productos llego al limite del Array,enter para continuar");
                        Console.ReadKey(); // captura cualquier tecla
                        Console.Clear();// Limpia la consola
                        subMenu = true; // salir del ciclo
                        return;
                    }




                    Console.Write("Ingrese nombre del Producto: ");
                    string nombre = Console.ReadLine();

                    Productos pd = new Productos { Id = GuadarDatos.tamanioProducto(), Nombre = nombre };

                    GuadarDatos.GuardarProducto(pd);

                    Console.Write("Desea Ingresar Otro Producto (S/N): ");
                    opcion = Char.Parse(Console.ReadLine());

                    if (opcion == 'N' || opcion == 'n') { subMenu = true; Console.Clear(); }




                }
                catch (Exception e)
                {
                    Console.Write("Se presento el siguiente error : " + e.Message + "\n\n Prsione cualquier tecla para continuar");
                    Console.ReadKey(); // captura cualquier tecla
                    Console.Clear();// Limpia la consola
                }


            } while (!subMenu);

        }

        //Metodo para Asignar Categorias al Producto
        static void AddAsignarCxP()
        {
            bool subMenu = false;
            do
            {
                Console.Clear();
                CategoriasXProductos CxP = new CategoriasXProductos();
                char opcion;

                try
                {
                    if (GuadarDatos.tamanioCategoria() == 0)
                    {

                        Console.Write("No hay Categorias Registradas, Vaya a la opcion 1 del Menu (enter): ");
                        Console.ReadKey(); // captura cualquier tecla
                        Console.Clear();// Limpia la consola
                        subMenu = true; // salir del ciclo
                        return;

                    }


                    if (GuadarDatos.tamanioProducto() == 0)
                    {

                        Console.Write("No hay Productos Registrados, Vaya a la opcion 2 del Menu (enter): ");
                        Console.ReadKey(); // captura cualquier tecla
                        Console.Clear();// Limpia la consola
                        subMenu = true; // salir del ciclo
                        return;

                    }

                   



                    if (GuadarDatos.tamanioCxP() >= 5) // de 0 a 4 hay 5 elementos
                    {
                        Console.Write("No se puede asignar  mas Categoria a Productos llego al limite del Array,enter para continuar");
                        Console.ReadKey(); // captura cualquier tecla
                        Console.Clear();// Limpia la consola
                        subMenu = true; // salir del ciclo
                        return;
                    }

                    // se pone la asignacion automaticamente

                    CxP.IdAsignacion = GuadarDatos.tamanioCxP();

                    Console.WriteLine("**** Categorias Disponible ****\n");

                    Categorias[] listaC = GuadarDatos.listaC();

                    for (int i = 0; i < listaC.Count(x => x != null); i++)
                    {
                        Console.WriteLine("ID: " + listaC[i].IdC + " - " + listaC[i].Nombre);
                    }

                    bool submenu2 = false;

                    do
                    {
                        Console.Write("\nSeleccione un ID de Sucursal: ");
                        int id = Int32.Parse(Console.ReadLine());

                        Categorias c2 = GuadarDatos.BuscarC(id);
                        if(String.IsNullOrEmpty(c2.Nombre)) // si es nulo o vacio no existe la categoria en la lista
                        {
                            Console.Write("Categoria No Existe\n");

                        } 
                        else // caso contrario se sale del do while y se guadar la categoria
                        {
                            submenu2 = true;
                            CxP.Categoria = c2;
                        }

                    } while (!submenu2);


                    Console.WriteLine("\n**** Productos Disponible ****\n");
                    Productos [] listaP = GuadarDatos.listaP();

                    for (int a = 0; a < listaP.Count(x => x != null); a++) {

                        Console.WriteLine("ID: " + listaP[a].Id + " - " + listaP[a].Nombre);

                    }

         

                    bool submenu3 = false;
                    int contarPA = 0; // contar los Productos registrados a la categoria maximo 5

                    do
                    {
                        if(contarPA < 5)
                        {
                            bool subMenu4 = false;

                            do
                            {

                                Console.Write("\nSeleccione un ID del Producto para la Categoria de "+CxP.Categoria.Nombre+" : ");
                                int id = Int32.Parse(Console.ReadLine());

                                Productos p2 = GuadarDatos.BuscarP(id);
                                if (String.IsNullOrEmpty(p2.Nombre)) // si es nulo o vacio no existe el producto en la lista
                                {
                                    Console.Write("Producto No Existe\n");

                                }
                                else // caso contrario se sale del do while y se guadar los productos
                                {

                                    CxP.Producto[contarPA] = p2;
                                    contarPA++;
                                    subMenu4 = true;
                                }

                            } while (!subMenu4);

                            


                            Console.Write("Desea Ingresar otro Producto a esta Categoria (S/N): ");
                            opcion = Char.Parse(Console.ReadLine());

                            if (opcion == 'N' || opcion == 'n') { submenu3 = true; }




                        }
                        else
                        {
                            Console.Write("\nNo se puede asignar  mas Producto a esta Categoria ya llego al limite del Array,enter para continuar");
                            Console.ReadKey(); // captura cualquier tecla
                            submenu3 = true;
                        }



                     
                    } while (!submenu3);


                    // se agrega la asignacion
                    GuadarDatos.GuardarCxP(CxP);

                    // se le indica si va hacer otra referencia
                    Console.Write("\nDesea realizar otra Asignacion (S/N): ");
                    opcion = Char.Parse(Console.ReadLine());

                    if (opcion == 'N' || opcion == 'n') { subMenu = true; }




                }
                catch (Exception e)
                {
                    Console.Write("\nSe presento el siguiente error : " + e.Message + "\n\n Prsione cualquier tecla para continuar");
                    Console.ReadKey(); // captura cualquier tecla
                    Console.Clear();// Limpia la consola
                }


            } while (!subMenu);

        }

        // Metodo para Listar la Categorias referenciada a los productos.
        static void ListarCxP()
        {
            CategoriasXProductos[] ListaCxP = GuadarDatos.ListaCxP();

            if(GuadarDatos.tamanioCxP() == 0)
            {
                Console.Write("No hay Datos registrados - enter para continuar: ");
                Console.ReadKey();
                return;
            }

            Console.WriteLine("*** Lista de Categorias por  Producto ****");
            for (int i = 0; i < ListaCxP.Count(x => x != null); i++)
            {
                Console.WriteLine("\n---------------------------------------");
                Console.WriteLine("Registro: " + ListaCxP[i].IdAsignacion);
                Console.WriteLine("Categoria: " + ListaCxP[i].Categoria.IdC + " - " + ListaCxP[i].Categoria.Nombre);
                Console.WriteLine("Productos Registrados: ");

                

                for(int a = 0; a < ListaCxP[i].Producto.Count(x => x != null); a++)// el x => x != null evita erroes de referenica nula
                {
                    Console.WriteLine((a + 1) + ". " + ListaCxP[i].Producto[a].Nombre+".");
                }

                
            }

            Console.WriteLine("\n********** Fin de la Lista *******\n");
            Console.Write("Presione cualquie tecla para salir: ");
            Console.ReadKey();

        }
    }
}
