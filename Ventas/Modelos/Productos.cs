﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ventas.Modelos
{
    internal class Productos
    {
        public int Id { get; set; } // id del producto
        public string Nombre { get; set; } // nombre del producto

    }
}
