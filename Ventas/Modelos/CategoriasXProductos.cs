﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ventas.Modelos
{
    internal class CategoriasXProductos
    {
        public int IdAsignacion { get; set; }
        public Categorias Categoria { get; set; }
        public Productos[] Producto { get; set; } = new Productos[5];  
    }
}
