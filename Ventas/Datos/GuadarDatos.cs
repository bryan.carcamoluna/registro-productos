﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ventas.Modelos; // Si utilizan carpetas para guardar las clase debe llamarlas como Using nombreproyecto.nombreCarpeta


namespace Ventas.Datos
{
     internal class GuadarDatos // se pone static para acceder desde cualquier clase
    {
        // Crear los arreglos para guardar Datos
        private static CategoriasXProductos[] ArregloCxP = new CategoriasXProductos[5]; //arreglo de tamanio 5
        private static Categorias[] ArregloC = new Categorias[5];
        private static Productos[] ArregloP = new Productos[5];

        //Crear los contadores para llevar la cantidad de registro
        private static int contarCategoria = 0; // Para que lleve el conteo de las Categoria maximo 5 de acuerdo al tamaño del Array
        private static int contarProducto = 0; // Para que lleve el conteo de las Producto maximo 5 de acuerdo al tamaño del Array
        private static int contarCxP = 0; // Para que lleve el conteo de la asignacion de categoria por producto maximo 5 de acuerdo al tamaño del Array
        


        // Metodo para agregar la categorias
        public static void GuardarCategoria( Categorias catg)
        {
            ArregloC[contarCategoria] = catg; // se agrega una categoria
            contarCategoria++;
        }

        //Metodo que devuelve el valor de cuantas categorias se han ingresado
        public static int tamanioCategoria()
        {
            return contarCategoria;
        }

        // Metodo que retorna la lista de Categorias
        public static Categorias[] listaC()
        {
            return ArregloC;
        } 

        // Metodo para Buscar Una categoria
        public static Categorias BuscarC(int id)
        {
            Categorias temp = new Categorias();

            for (int i = 0; i < ArregloC.Count(x => x != null); i++)
            {
                if (ArregloC[i].IdC == id)
                {
                    temp = ArregloC[i];
                }

            }


            return temp;
        }


        // Metodo para agregar Productos

        public static void GuardarProducto(Productos prod )
        {
            ArregloP[contarProducto] = prod; // se agrega un Producto
            contarProducto++; // aumenta cada vez que se agrega un producto
        }


        //Metodo que devuelve el valor de cuantos Productos se han ingresado
        public static int tamanioProducto()
        {
            return contarProducto;
        }


        // Metodo devuelve la lista de los Productos
        public static Productos[] listaP()
        {
            return ArregloP;
        }

        // Metodo para Buscar un Producto
        public static Productos BuscarP(int id)
        {
            Productos temp = new Productos();

            for (int i = 0; i < ArregloP.Count(x => x != null); i++)
            {
                if (ArregloP[i].Id == id)
                {
                    temp = ArregloP[i];
                }

            }

            return temp;
        }




        //Metodo guarda las asignacion de Categoria por Producto
        public static void GuardarCxP(CategoriasXProductos CxP)
        {
            ArregloCxP[contarCxP] = CxP; // se agrega una categoria
            contarCxP++;
        }


        //Metodo que devuelve el valor de cuantos Productos se han ingresado
        public static int tamanioCxP()
        {
            return contarCxP;
        }


        // devolver la lista de Categoria x Producto para imprimiar en consola

        public static CategoriasXProductos[] ListaCxP()
        {
            return ArregloCxP;
        }
    }
}
